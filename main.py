import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk
import os

class ImageViewer(tk.Tk):

    def __init__(self):
        super().__init__()

        # Alapértelmezett ablakméret és pozíció beállítása
        self.geometry("800x600")
        self.center_window()

        self.title("Képnézegető")

        # Keret a kép és státusz sáv számára
        self.main_frame = tk.Frame(self)
        self.main_frame.pack(expand=True, fill=tk.BOTH)

        self.image_label = tk.Label(self.main_frame, bg="black")
        self.image_label.grid(row=0, column=0, sticky=tk.NSEW)

        self.original_image = None
        self._after_id = None

        # Menü létrehozása
        menu = tk.Menu(self)
        file_menu = tk.Menu(menu, tearoff=0)
        file_menu.add_command(label="Megnyitás", command=self.open_image)
        file_menu.add_command(label="Kilépés", command=self.quit)
        menu.add_cascade(label="Fájl", menu=file_menu)
        self.config(menu=menu)

        # Status bar létrehozása
        self.status_bar = tk.Label(self.main_frame, relief=tk.SUNKEN, anchor=tk.W, bd=1)
        self.status_bar.grid(row=1, column=0, sticky=tk.EW)

        # Az elrendezési súly beállítása, hogy az image_label töltsön ki több teret
        self.main_frame.grid_rowconfigure(0, weight=1)
        self.main_frame.grid_columnconfigure(0, weight=1)

        self.image_label.bind("<Configure>", self.schedule_resize)

        # A képek listája és az aktuális kép indexe
        self.image_list = []
        self.current_image_index = None

        # Gombokhoz események hozzáadása
        self.bind("<Left>", self.prev_image)
        self.bind("<Right>", self.next_image)

    def center_window(self):
        self.update_idletasks()
        x = (self.winfo_screenwidth() - self.winfo_width()) // 2
        y = (self.winfo_screenheight() - self.winfo_height()) // 2
        self.geometry(f"+{x}+{y}")

    def open_image(self):
        raw_file_path = filedialog.askopenfilename(
            filetypes=[("Képfájlok", "*.png;*.jpg;*.jpeg;*.bmp;*.gif"), ("Minden fájl", "*.*")])
        file_path = os.path.normpath(raw_file_path)  # Normalizálja az elérési utat

        if not file_path:
            return

        # Képek listázása a megnyitott kép könyvtárából
        directory = os.path.dirname(file_path)
        for filename in sorted(os.listdir(directory)):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif')):
                normalized_path = os.path.normpath(os.path.join(directory, filename))
                self.image_list.append(normalized_path)

        print("Megnyitott fájl elérési útja:", file_path)
        print("Képek listája:")
        for i, img in enumerate(self.image_list):
            print(i, img)

        if file_path in self.image_list:
            self.current_image_index = self.image_list.index(file_path)
        else:
            print("Hiba: A megnyitott kép nem található a listában.")
            return

        self.original_image = Image.open(file_path)
        img_width, img_height = self.original_image.size
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()

        # Status bar frissítése
        self.update_status_bar()

        # Adjust the image dimensions if they exceed the screen size
        if img_width > screen_width or img_height > screen_height:
            self.original_image.thumbnail((screen_width, screen_height), Image.LANCZOS)

        self.geometry(f"{self.original_image.width}x{self.original_image.height}")
        self.center_window()
        self.resize_and_display_image()

    def load_image_by_index(self):
        if self.image_list and self.current_image_index is not None:
            self.original_image = Image.open(self.image_list[self.current_image_index])
            self.resize_and_display_image()
            self.update_status_bar()

    def resize_and_display_image(self):
        max_width = self.image_label.winfo_width()
        max_height = self.image_label.winfo_height()

        if self.original_image:
            image = self.original_image.copy()
            image.thumbnail((max_width, max_height), Image.LANCZOS)
            photo = ImageTk.PhotoImage(image)
            self.image_label.configure(image=photo)
            self.image_label.image = photo

    def schedule_resize(self, event):
        if self._after_id:
            self.after_cancel(self._after_id)
        self._after_id = self.after(50, self.resize_and_display_image)

    def update_status_bar(self):
        original_resolution = f"Original Resolution: {self.original_image.width}x{self.original_image.height}"
        current_resolution = f"Current Window Size: {self.winfo_width()}x{self.winfo_height()}"
        self.status_bar.config(text=f"{original_resolution} | {current_resolution}")

    def prev_image(self, event=None):
        if self.image_list and self.current_image_index is not None:
            self.current_image_index = (self.current_image_index - 1) % len(self.image_list)
            self.load_image_by_index()

    def next_image(self, event=None):
        if self.image_list and self.current_image_index is not None:
            self.current_image_index = (self.current_image_index + 1) % len(self.image_list)
            self.load_image_by_index()

if __name__ == "__main__":
    app = ImageViewer()
    app.mainloop()
